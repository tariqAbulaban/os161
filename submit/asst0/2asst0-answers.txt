commit 34190bc2a852ed9bf644d7615f6614558b2b6fba
Author: tariqabulaban <tariq__23@hotmail.com>
Date:   Tue Sep 11 18:09:13 2018 -0700

    Initial commit of os161 for CPEN331


1.  In the book chapters and in class you were introduced to the 
mechanisms used to transfer control between user processes and the
 operating system. Tell us where we can find the first line of OS/161
  code that is executed when a trap occurs. Then tell us where control 
  gets transferred to from that point. What about an interrupt? 
  How does that differ?

the first line of code will be based on the type of exception it is, i.e. if it is a 
utlb exception then the first line will be on like 68, otherwise if its a 
general exception then the first line is line 86 "mips_general_handler".

From there they both call common_exception then we are eventually sent to the jal mips_trap.
in mips_trap() in trap.c, it distinguishes if it is a interrupt or a syscall.
  mips_trap gets called by jal mips_trap on line 260 in exception-mips1.s.
  From there, a trap is handled by the mips_trap function in trap.c.

If it is a syscall, mips_Trap calls syscall(args) and if it is an interrupt, it calls
mainbus_interrupt. Therefore, they differ by the way they are transferred/sent out. 

2. syscall gets called on line 84 of userland/lib/libc/arc/mips/syscall-mips.S

3. There are library functions in common/libc so that both the kernal and the user can have libraccess to
these important and useful libaraies and not just one of them. And in userland, the libraries in there
are specfic only to the user.


4. 2 things configure configures are 1) the target hardware platorm and 2) the machine type.

5.  Constants:
    Types of MIPS32 like typedef __u32 paddr_t;
    MIPS machine-dependent definitions for the ELF binary format. EM_MACHINE
    mips32 is different than MIPS-I for hardwired memory layour
    Machine dependent VM system definitions such as PAGE_SIZE and PAGE_FRAME

    Above I have noted some machine dependent constants. The functions will make use of
    these constants and perform their actions based on the values that are given to them.
    This allows the functions to perform the low level actions needed that would only work
    on the specified type of machine. 
    Certain functions will need to know , for example, which registers we can use and their locations.

    By maintaining the separation, we can better debug and organize our code in order to maintain stability,
    and if we need to reuse the code somewhere else, we know which machine dependent functions we must change.

6.  There are 37 4-byte unsigned integers so therefore 37*4 btes = 148 total bytes used 
    Stores 37 registers: vaddr, status,cause, hi, lo, epc etc...

7. must re-run the config script if:
    - we change the configure name or config file
    - we change/add/remove devices/attachments
    - 

8. run bmake depend when you change, add, or remove any of the #include in any of your code files
   because bmake depend will setup all the dependencies for your MAKEFILE and allow it functions
   properly.

9. we run bmake install everytime reinitialize/recompile our kernal when we make
    any changes to our kernal.

10. We can create a function in the kern/menu.c file which would be similar to some of 
the already provided functions.
hello world can look like: 
static
void
cmd_helloWorld()
{
	kprintf("Hello world! \n");
	return;
}
and add "[hw] print hello world" into the opsmenu[] array
and add to the cmdtable[] array something like {"hw", cmd_helloWorld}

11. we need to include these because our OS runs on sys161 which is a virtual machine, 
when we use a virtual machine, and since it simulates a MIPS like design, it may not be set up
to be able to use the hosts utilities due to them not being compiled and configured to
MIPS.

12. after exiting, the return value gets set into register s0 which is a callee save
 so we still have it in case exit() returns, and a0 so it is an argument to exit.

13.

We would need to modify or add code in the following:

1) kern/include/kern/syscall.h: we need to add a #define for our new system call with its own distinct number.
2) add a function or .c file in kern/syscall so that the syscall has code to run when triggered.
3) add function prototype into kern/include/syscall.h
4) add case into switch statement for the new system call to get handled in kern/arch/mips/syscall/syscall.call

to test the new system call we must re-configure the program, bmake depend, bmake and bmake install. 



