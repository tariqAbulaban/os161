Code reading exercises:

1. 
For thread Exit : 
- it Detaches the thread from our process
- assert that it is indeed detached.
- checks the stack guard band
- turns off interrupts for this processor
- switches the thread.

for thread sleep: 
- releases the lock.
- calls threadSwitch to switch to another thread passing indeed
S_SLEEP which queues the thrad onto a wait channel wc.
- reacquires the lock and continues to run


2. Thread_switch() is the function that handles the context switching
and inside of thread_switch, I noticed a function called switchframe_switch()
that directly deals with the assembly language and does the switch in assembly.

3.

There are 4 states that a thread could be in:
S_RUN which is running
S_READY which means ready to run
S_SLEEP which means sleeping, i.e. it has given up the lock
    but expects to be woken up eventually. 
S_ZOMBIE which means it is exited but not deleted 

4. i) What does it mean to turn interrupts off?
 ii) How is this accomplished? 
 iii) Why is it important to turn off interrupts in the thread subsystem code?

i) 
what it means to turn interrupts off is that if an interrupt was 
signaled to the handler, the handler cant do anything about it until interrupts 
are back on.
ii)
this is accomplished by calling splhigh  
iii)
it is important to turn off interrupts because it allows our thread in the spinlock or semaphore
to do its job without being interrupted by another thread. i.e. allows the thread to complete and not get 
"interrupted" in the middle of it.
This allows our thread to be atomic

5. 
we take the sleeping thread off the waiting channel and then we input the sleeping thread into thread_make_runnable() which 
gets the target thread ready and puts it into the end of the runqueue.


6. 
thread_make_runnable adds it to the end of the runqueue then is manipulated by :
- Thread_switch() which selects the next queue to go.
- schedule() (does nothing though)
- thread_consider_migration manages which CPU the threads should be 
depending on the state of the cpu (idle, less busy...)


7. How does it (do they) pick the next thread?
currently the "threads will run in a round-robin fashion" by thread_Switch() getting 
the next thread from the runqueue.

8.
What role does the hardware timer play in scheduling? What hardware independent function is called on a timer interrupt?

The hardclock (hardware timer) calls  thread_consider_migration(), and schedule() periodically.
These are important functions that i have talked about in question 6.
It calls thread_yield at the end which yields the cpu to another process, but makes it stay runnable.


9. Describe how wchan_sleep() and wchan_wakeone()
 are used to implement semaphores.

Wchan_sleep() is called inside of while loop in the P function.
The wchan_sleep() puts a thread to sleep until until the semaphores count is != 0.
wchan_wakeone is called in the V function and acts a function to wake one single thread from sleep and then the the semaphores count is incremented.

10. 

from the documentation of wchan, wchan is protected by an associated, passed-in spinlock.
A spinlock is basically a busy wait so that allows the thread to go to sleep fully and get placed into the sleep queue, before another thread can attempt to awake the first thread.
By acquiring this spinlock, no other thread can use the wait channel. It calls thread_switch which turns off interrupts which acts a way to prevent any other thread to use the resources that are currently being used and allows the current thread to finish its job of being added into the sleep channel.












