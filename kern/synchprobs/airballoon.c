/*
 * Driver code for airballoon problem
 */
#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>
#include <current.h>

#define NROPES 16
static int ropes_left = NROPES;

/* function protoypes */

bool lock_acquireForAirballoon(struct lock *lock);
struct rope * rope_create(unsigned id, unsigned stakeID );
void rope_destroy(struct rope *rope);
void swap(unsigned *a, unsigned *b);
void knuthShuffle(unsigned *array, unsigned n );
void randomize_map(void) ;

/*
 * A modified lock acquire function that returns immediately when lock is not acquirable 
 * instead of putting the thread to sleep.
 * 
 * @params: takes in a lock.
 */ 


bool lock_acquireForAirballoon(struct lock *lock)
{
        KASSERT(lock != NULL);
        KASSERT(curthread->t_in_interrupt == false);

	spinlock_acquire(&lock->lk_lock);
        if (!lock->lk_free) {
			spinlock_release(&lock->lk_lock);
	        return false;
        }
        
        KASSERT(lock->lk_free == true);
        lock->lk_free = false;
        lock->holder = curthread;
	spinlock_release(&lock->lk_lock);
	return true;
}

struct lock *ropesCountLock;
struct lock *exitCondLock;
struct cv *cv; 

struct rope{
	volatile unsigned stakeID;
	unsigned id;
	struct lock *lock;
	bool isConnected;

};


/* Rope Create
 *
 * Creates a new rope by allocating memory and initializes all its variables
 * in the rope struct
 * 
 */

struct rope * rope_create(unsigned id, unsigned stakeID )
{
    struct rope *rope;

    rope = kmalloc(sizeof(struct rope));
    if (rope == NULL) {
        return NULL;
    }

    rope->id = id;
		
    if (rope->id != id) {
        kfree(rope);
        return NULL;
    }

	rope->stakeID = stakeID;
	rope->isConnected = true;
        return rope;
}
/*
 * Deallocates the rope memory and destroys the rope lock. 
 */ 

void rope_destroy(struct rope *rope)
{
        KASSERT(rope != NULL);

		lock_destroy(rope->lock);
		kfree(rope);
}

// stake and hook abstractions as arrays 
volatile unsigned stakes[NROPES];
volatile unsigned hooks[NROPES]; 

struct rope *ropes[NROPES];


/*
 * Knuth shuffle algorithm gotten from https://www.rosettacode.org/wiki/Knuth_shuffle
 */ 

void knuthShuffle(unsigned *arr, unsigned n )
{
	unsigned i;
	unsigned index;
	for( i = 0; i < n; i++){
		index = i + (random() %(n-i));
		swap(&arr[i], &arr[index]);
	}
}

void swap(unsigned *a, unsigned *b)
{
	unsigned temp;

	temp = *a;
	*a = *b;
	*b = temp;
}

enum character
{
	DANDELION,
	MARIGOLD,
	FLOWERKILLER,
};

/* printDeletion function
 *  
 * Prints out a message to the kernel depending on who is calling it.
 * 
 * @params: the character who that is calling this function, 
 * 			startingStakeID: the Oringial Stake Position
 * 			movedToStake: if the stake has moved, this is its new position
 */ 
static
void
printDeletion(enum character who,
                      unsigned startingStakeID,
					  unsigned movedToStake,
                      unsigned ropeID) 
{

	if(who == DANDELION){
		kprintf("Dandelion severed rope %u\n", ropeID);
	}
	else if(who == MARIGOLD){

		kprintf("Marigold severed rope %u from stake %u\n", ropeID, startingStakeID);

	}else{
		kprintf("Lord FlowerKiller switched rope %u from stake %u to stake %u\n", ropeID, startingStakeID,movedToStake);

	}
	thread_yield(); 
}
/* randomize_map function

 * Initializes Two locks: ropesCountLock and exitCondLock.
 * creates an array of random numbers and sets initializes the hook/rope IDs and stakes IDs 
 * 
 * Initializes and fills the ropes and stakes arrays.
 */
void randomize_map(void) 
{
	unsigned randomNumbersArray[NROPES];
	unsigned i;
	
	ropes_left = NROPES;
	for( i = 0 ; i < NROPES; i++ ){
		randomNumbersArray[i] = i;
	}

	// knuthShuffle(randomNumbersArray, NROPES); //shuffles the array 

	// creation and initialization of lock of rope and exit locks
	ropesCountLock = lock_create("ropes count lock"); 
	exitCondLock = lock_create("exit condition lock");
	cv = cv_create("condition variable");

	for(i = 0; i < NROPES; i++){

		struct rope *rope = rope_create(randomNumbersArray[i], i);
		rope->lock = lock_create("rope lock");
		KASSERT(rope != NULL);
		
		//initializing abstraction arrays
		hooks[i] = i;
		ropes[i] = rope;
		stakes[i] = i;
		
	}
	
}


/*
 * Describe your design and any invariants or locking protocols 
 * that must be maintained. Explain the exit conditions. How
 * do all threads know when they are done?  
 */


/*
 * Dandelion function 
 *
 * Dandelion generates a random hook/rope id and then locks the rope. 
 * when he locks the rope, he severs the rope from the hook by setting its
 * isConnected variable to be false, and severs the rope from the stake by 
 * setting the stake ID to be -1 representing disconnect.
 * 
 * Exit condition : once ropes_left = 0, it breaks out of the loop and 
 * thread exits.
 */ 
static
void
dandelion(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Dandelion thread starting\n");

	int randomNum = random() % NROPES;
	while(1){
		
		if(ropes_left == 0){
			break;
		}
		unsigned ropeToGet = hooks[randomNum];
		volatile struct rope *rope = ropes[ropeToGet];

		if(lock_acquireForAirballoon(rope->lock)){
			if(rope->isConnected){
				//severing the rope from the hook 
				rope->isConnected = false;

				//severing the rope from the stake 
				rope->stakeID = -1;

				lock_release(rope->lock);
				printDeletion(DANDELION,1,1, rope->id);

				// locks the ropes_left variable and decrements the counter.
				lock_acquire(ropesCountLock);
				ropes_left--;
				lock_release(ropesCountLock);			
				}

				lock_release(rope->lock);

		}
	
			randomNum = random() % NROPES;
		}
		
		kprintf("Dandelion thread done\n");
		
		thread_exit();
		
	
}
/*
 * Marigold function 
 *
 * Marigold generates a random stake id and then searches through the rope 
 * array and locks the a rope that is holding that stake id in it. 
 * when she locks the rope, she severs the rope from the hook by setting its
 * isConnected variable to be false, and severs the rope from the stake by 
 * setting the ropes stake ID to be -1 representing disconnect.
 * 
 * Exit condition : once ropes_left = 0, it breaks out of the loop and 
 * thread exits.
 */ 

static
void
marigold(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Marigold thread starting\n");

	unsigned i;

	while(1){
		int randomNum = random() % NROPES;
		
		lock_acquire(ropesCountLock);
		if(ropes_left == 0){ // exit condition
			lock_release(ropesCountLock);
			break;
		}
		lock_release(ropesCountLock);


		unsigned stakeID =  stakes[randomNum];
		volatile struct rope *rope;

		for(i = 0; i < NROPES; i++ ){
			if(ropes[i]->stakeID == stakeID){
				rope = ropes[i];
				break;
			}

		}
		if(i == NROPES){
			continue; // no rope was found for this stake;
		}
		
		if(lock_acquireForAirballoon(rope->lock)){
			if(rope->isConnected){
				rope->isConnected = false;
				rope->stakeID = -1;
				lock_release(rope->lock);

				printDeletion(MARIGOLD,stakeID,1, rope->id);
				// surround with locks;
				lock_acquire(ropesCountLock);
				ropes_left--;
				lock_release(ropesCountLock);
			}
			lock_release(rope->lock);
		}
	
	}
	
	kprintf("Marigold thread done\n");

	thread_exit();

}
/*
 * flowerKiller function 
 *
 * flowerKiller generates 2 ranodm stake ids, stakeFrom and stakeTo.
 * He finds the rope that is holding the stakeFrom id and then sets that ropes
 * stakeID to be stakeTo.
 * 
 * Exit condition : once ropes_left = 0, it breaks out of the loop and 
 * thread exits.
 */ 
static
void
flowerkiller(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Lord FlowerKiller thread starting\n");

unsigned i;
	while(1){
		unsigned stakeFromIndex = random() % NROPES;
		unsigned stakeToIndex;

		do{
		stakeToIndex = random() % NROPES;

		}while(stakeFromIndex == stakeToIndex);

		lock_acquire(ropesCountLock);
		if(ropes_left == 0){
			lock_release(ropesCountLock);
			break;
		}
		lock_release(ropesCountLock);
	

		unsigned stakeFrom = stakes[stakeFromIndex];
		unsigned stakeTo = stakes[stakeToIndex];
		volatile struct rope *rope;

		for(i = 0; i < NROPES; i++ ){ // searches for the rope that holds the stakeFrom index.
			if(ropes[i]->stakeID == stakeFrom){
						rope = ropes[i];
						break;
					}

		}
		if(i == NROPES){
			continue; // no rope was found for this stake;
		}
		
		if(lock_acquireForAirballoon(rope->lock)){
			if(rope->isConnected){
				rope->stakeID = stakeTo;

				printDeletion(FLOWERKILLER,stakeFrom,stakeTo, rope->id);
				lock_release(rope->lock);

			}
			lock_release(rope->lock);	
		}
	}

	// all threads have finished, free the memory for the rope array 
	// and destroy locks by rope destroy.
	for(i = 0; i < NROPES; i++){
		rope_destroy(ropes[i]);
	}

	kprintf("Lord FlowerKiller thread done\n");

	
	thread_exit();
}
/* Balloon function
 * 
 * Checks the count of ropes left. 
 * Once the exit condition is true, it prints out that the thread
 * and Dandelion is free.
 *  
 */
static
void
balloon(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Balloon thread starting\n");
	
	lock_acquire(ropesCountLock);

	while(ropes_left > 0){
		lock_release(ropesCountLock);
		lock_acquire(ropesCountLock);
	}

	lock_release(ropesCountLock);

	kprintf("Balloon freed and Prince Dandelion escapes!\n");
	kprintf("Balloon thread done\n");

	cv_signal(cv, exitCondLock); // wake up the main thread to finish.
	thread_exit();
	
	
}


// Change this function as necessary
int
airballoon(int nargs, char **args)
{

	int err = 0;

	(void)nargs;
	(void)args;
	(void)ropes_left;
	randomize_map();
	
	err = thread_fork("Marigold Thread",
			  NULL, marigold, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Dandelion Thread",
			  NULL, dandelion, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Lord FlowerKiller Thread",
			  NULL, flowerkiller, NULL, 0);
			  
	if(err)
		goto panic;

	err = thread_fork("Air Balloon",
			  NULL, balloon, NULL, 0);
			  
	if(err)
		goto panic;

	
	goto done;
panic:
	panic("airballoon: thread_fork failed: %s)\n",
	      strerror(err));
	
done:
	lock_acquire(ropesCountLock);


	while(ropes_left > 0){
		lock_release(ropesCountLock);
		lock_acquire(ropesCountLock);
	
	}
	lock_release(ropesCountLock);

	lock_acquire(exitCondLock);

	cv_wait(cv, exitCondLock); // go to sleep until woken up

	cv_destroy(cv); // deallocate memory for the condition variable
	
	// clean memory of locks.
	lock_destroy(ropesCountLock);
	lock_destroy(exitCondLock);

	kprintf("Main thread done\n"); 

	

	return 0;
}
